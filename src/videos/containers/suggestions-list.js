import React, {Component} from 'react';

import {
    FlatList
} from 'react-native';

import Layout from '../components/suggestion-list-layout';
import Empty from '../components/Empty';
import Separator from '../components/vertial-separator';
import Suggestion from '../components/suggestion';

class SuggestionList extends Component {

    renderEmpty = () => <Empty text="No hay sugerencias"/>;
    itemSeparator = () => <Separator />;
    renderItem = ({ item }) => { return ( <Suggestion {...item} /> ) };

    render() {

        const list = [
            {
                key: '1',
                title: 'averges'
            },
            {
                key: '2',
                title: 'Pokemon'
            }


        ];

        return (
            <Layout title='Recomendado para ti'>

                <FlatList
                    data={list}
                    ListEmptyComponent={this.renderEmpty}
                    ItemSeparatorComponent={this.itemSeparator}
                    renderItem={this.renderItem}
                />
            </Layout>
        );
    }
}

export default SuggestionList;
